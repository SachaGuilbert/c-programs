#include <stdio.h>

int fun(int n){
    int r = 0;
    while (n>0) {
        n = n/2;
        r = r + 1;
    }
    return r;
}
int main() {
    int n = 1;
    for(int i = 0; i < 10; i++) {
        printf("%10d %10d\n", n, fun(n));
        n = n*10;
    }
    return 0;
}
