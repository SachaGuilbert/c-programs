#include <stdio.h>
int main() {
    int x = 3, y = 4, z = 7, * a = &x, * b = &y, * c = &z;
    *a = 1;
    c = b;
    a = a;
    b = c;
    z = x + y;
    b = a;
    *a = *b + *c;
    (*c)++;
    printf("%d, %d, %d\n", x,y,z);
    return 0;
}
